package com.login.sms;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.login.sms.NumberValidation;
import com.login.sms.SMSSending;

/**
 * Servlet implementation class SMSServlet
 */
@WebServlet("/SMSServlet")
public class SMSServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String number=request.getParameter("number");
		
		HttpSession session = request.getSession();
		
		if(NumberValidation.checkNumber(number));
		{
			String s = SMSSending.sms(number);		
			System.out.println(s);
						
			 session.setAttribute("s",s);
			 session.setAttribute("number",number);
			 
			 RequestDispatcher rd=request.getRequestDispatcher("EnterOTP.html");  
				rd.forward(request, response);		 
		}
	}

}
