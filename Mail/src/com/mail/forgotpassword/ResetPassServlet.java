package com.mail.forgotpassword;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Base64;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mail.forgotpassword.ForgotPassValidate;
import com.project.gMail.DbCon;

/**
 * Servlet implementation class ResetPassServlet
 */
@WebServlet("/ResetPassServlet")
public class ResetPassServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection con = null;
		PreparedStatement pstmt = null;
		int i = 0;
		
		String password = request.getParameter("password");
		String emailid = request.getParameter("email");
		
		password = Base64.getEncoder().encodeToString( password.getBytes("utf-8"));
		 System.out.println("Base64 Encoded String (Basic) :" + password);
		 
	    HttpSession session = request.getSession();
		session.setAttribute("emailid",emailid);
		 
		String query="update regestration set password=?where emailid=?";
		ForgotPassValidate.checkMail(emailid);
		{
		try
		{
			con=DbCon.connection();
			
			pstmt=con.prepareStatement(query);
			 
			pstmt.setString(1, password);
			pstmt.setString(2, emailid);
			
			i=pstmt.executeUpdate();
			
			System.out.print(i);
			if(i>0)
			{
				RequestDispatcher rd=request.getRequestDispatcher("SuccessMail");  
				rd.forward(request, response);	
			}
			else
			{
				RequestDispatcher rd=request.getRequestDispatcher("UnSuccessMail");  
				rd.forward(request, response);	
			}
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}
	}

}
