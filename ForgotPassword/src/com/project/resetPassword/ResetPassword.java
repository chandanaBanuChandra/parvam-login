package com.project.resetPassword;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.forgotPassword.ForgotPasswordValidate;
import com.project.login.DbCon;

/**
 * Servlet implementation class ResetPassword
 */
@WebServlet("/ResetPassword")
public class ResetPassword extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection con = null;
		PreparedStatement pstmt = null;
		int i = 0;
		
		String password = request.getParameter("password");
		String emailid = request.getParameter("email");
		
		String query="update regestration set password=?where emailid=?";
		ForgotPasswordValidate.checkMail(emailid);
		{
		try
		{
			con=DbCon.connection();
			
			pstmt=con.prepareStatement(query);
			 
			pstmt.setString(1, password);
			pstmt.setString(2, emailid);
			
			i=pstmt.executeUpdate();
			
			System.out.print(i);
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}
	}

}
