package com.project.register;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterSetGet
 */
@SuppressWarnings("serial")
@WebServlet("/RegisterSetGet")
public class RegisterSetGet extends HttpServlet {

		private String username;
		private String emailid;
		private String password;	
		private String address;
		private String phonenumber;               
		
		public String getusername()
		{
			return username;
		}
		public void setusername(String username)
		{
			this.username=username;
		}
		
		public String getemailid()
		{
			return emailid;
		}
		public void setemailid(String emailid)
		{
			this.emailid=emailid;
		}
		
		public String getphonenumber()
		{
			return  phonenumber;
		}
		public void setphonenumber(String phonenumber)
		{
			this.phonenumber= phonenumber;
		}
		
		public String getaddress()
		{
			return address;
		}
		public void setaddress(String address)
		{
			this.address=address;
		}
		
		public String getpassword()
		{
			return  password;
		}
		public void setpassword(String password)
		{
			this.password= password;
		}
		 
}
