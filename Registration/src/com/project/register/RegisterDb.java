package com.project.register;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Base64;

import com.project.register.DbCon;

public class RegisterDb 
{
	public static String regDatabase(RegisterSetGet rsg) throws UnsupportedEncodingException
	{			
		String username=rsg.getusername();
		String emailid=rsg.getemailid();
		String address=rsg.getaddress();
		String phone_number=rsg.getphonenumber();
		String password=rsg.getpassword();

	String q="insert into regestration(username,password,emailid,phone_number,address) values(?,?,?,?,?)";
		
	Connection con=null;
	PreparedStatement pstmt=null;
	
	int i=0;
	 String base64encodedString = Base64.getEncoder().encodeToString(password.getBytes("utf-8"));
	 System.out.println("Base64 Encoded String (Basic) :" + base64encodedString);
	 
	 
	try 
	{
		con=DbCon.connection();
	    pstmt=con.prepareStatement(q);
	    
	    pstmt.setString(1, username);
		pstmt.setString(2, base64encodedString);
		pstmt.setString(3, emailid);
		pstmt.setString(4, phone_number);
		pstmt.setString(5, address);
		
	   i=pstmt.executeUpdate();
	    
	   if(i!=0)
		{
			return "succes";
		}
		
	}
	catch(Exception e1)
	{
		e1.printStackTrace();
	}
	return "oops something went wrong";	
	}
}
