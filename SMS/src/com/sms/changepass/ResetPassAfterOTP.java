package com.sms.changepass;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sms.changepass.PasswordResetChangePass;
import com.sms.changepass.PasswordValidation;

/**
 * Servlet implementation class ResetPassAfterOTP
 */
@WebServlet("/ResetPassAfterOTP")
public class ResetPassAfterOTP extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String phone_number = request.getParameter("phone_number");
		 
		String password1 = request.getParameter("PerviousPassword");
		String password = request.getParameter("NewPassword");
		
		int i = 0;
		
		password = Base64.getEncoder().encodeToString( password.getBytes("utf-8"));
		 System.out.println("Base64 Encoded String (Basic) :" + password);
		 
		 HttpSession session = request.getSession();
		 phone_number = (String) session.getAttribute("phone_number"); 
		 
		 
		 if(PasswordValidation.checkPassword(password))
				{
		  i = PasswordResetChangePass.resetpassword(password, phone_number);
	        System.out.print(i);
				}
		 else
		 {
			 i = PasswordResetChangePass.resetpassword(password, phone_number);
		        System.out.print(i); 		 
	     }
	     if(i>0)
	     {
	    	 RequestDispatcher rd=request.getRequestDispatcher("SmsSuccess");  
				rd.forward(request, response);	
	     }
	     else
	     {
	    	 RequestDispatcher rd=request.getRequestDispatcher("SmsUnSuccess");  
				rd.forward(request, response);	
	     }
	}

}
