package com.sms.changepass;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class OtpValidationForChangePass
 */
@WebServlet("/OtpValidationForChangePass")
public class OtpValidationForChangePass extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		 String s = request.getParameter("res");
	 
		 HttpSession session = request.getSession();
		 s = (String) session.getAttribute("s"); 
		
		String otp = request.getParameter("OTP");
					
        otp = "";
			        System.out.print("succes");			    
		        RequestDispatcher rd=request.getRequestDispatcher("ChangePassAfterOtp.html");  
					rd.forward(request, response);	
		          }

}
